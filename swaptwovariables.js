let a = 5;
let b = 10;

console.log(`Before swap: a = ${a}, b = ${b}`);

// Using a temporary variable
let temp = a;
a = b;
b = temp;

console.log(`After swap: a = ${a}, b = ${b}`);
