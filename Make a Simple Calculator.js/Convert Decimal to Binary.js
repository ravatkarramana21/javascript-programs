function decimalToBinary(decimal) {
    let binary = '';
    let quotient = decimal;

    while (quotient > 0) {
        let remainder = quotient % 2;
        binary = remainder + binary;
        quotient = Math.floor(quotient / 2);
    }

    return binary || '0'; // Handle the case for 0 explicitly
}

// Example usage
let decimalNumber = 10;
let binaryNumber = decimalToBinary(decimalNumber);
console.log(`Decimal: ${decimalNumber} -> Binary: ${binaryNumber}`); // Decimal: 10 -> Binary: 1010
