const obj = { a: 1, b: 2 };

if ('a' in obj) {
    console.log("Key exists");
} else {
    console.log("Key does not exist");
}
