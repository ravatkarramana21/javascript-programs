function checkNumberSign(number) {
    if (number > 0) {
        return "Positive";
    } else if (number < 0) {
        return "Negative";
    } else {
        return "Zero";
    }
}

// Example usage
let number1 = 5;
let number2 = -3;
let number3 = 0;

console.log(`${number1} is ${checkNumberSign(number1)}`); // 5 is Positive
console.log(`${number2} is ${checkNumberSign(number2)}`); // -3 is Negative
console.log(`${number3} is ${checkNumberSign(number3)}`); // 0 is Zero
