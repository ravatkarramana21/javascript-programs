function getRandomArrayElement(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

// Example usage
let colors = ["red", "green", "blue", "yellow"];
let randomColor = getRandomArrayElement(colors);
console.log(randomColor); // Random element from the colors array
