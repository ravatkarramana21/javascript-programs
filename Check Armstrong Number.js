function isArmstrongNumber(number) {
    // Convert number to string and split into individual digits
    let digits = number.toString().split('');
    let numberOfDigits = digits.length;
    
    // Calculate the sum of each digit raised to the power of the number of digits
    let sum = digits.reduce((acc, digit) => acc + Math.pow(parseInt(digit), numberOfDigits), 0);
    
    // Check if the sum is equal to the original number
    return sum === number;
}

// Example usage
let number = parseInt(prompt("Enter a number to check if it's an Armstrong number:"));

if (isArmstrongNumber(number)) {
    console.log(`${number} is an Armstrong number.`);
} else {
    console.log(`${number} is not an Armstrong number.`);
}
