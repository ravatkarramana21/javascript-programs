
function findSquareRoot(num) {
    return Math.sqrt(num);
}

// Example usage
let number = 25;
let result = findSquareRoot(number);

console.log(`The square root of ${number} is ${result}`);
