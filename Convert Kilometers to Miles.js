function kilometersToMiles(kilometers) {
    if (typeof kilometers !== 'number' || kilometers < 0) {
        throw new Error('Please enter a valid non-negative number');
    }
    const conversionFactor = 0.621371;
    return kilometers * conversionFactor;
}

// Example usage
try {
    let kilometers = 10;
    let miles = kilometersToMiles(kilometers);
    console.log(`${kilometers} kilometers is equal to ${miles} miles`);
} catch (error) {
    console.error(error.message);
}
