function solveQuadratic(a, b, c) {
    // Calculate the discriminant
    let discriminant = b * b - 4 * a * c;
    
    // Check the nature of the roots
    if (discriminant > 0) {
        // Two real and distinct roots
        let root1 = (-b + Math.sqrt(discriminant)) / (2 * a);
        let root2 = (-b - Math.sqrt(discriminant)) / (2 * a);
        return `The roots are real and distinct: ${root1} and ${root2}`;
    } else if (discriminant === 0) {
        // One real root (repeated root)
        let root = -b / (2 * a);
        return `The root is real and repeated: ${root}`;
    } else {
        // Two complex roots
        let realPart = -b / (2 * a);
        let imaginaryPart = Math.sqrt(-discriminant) / (2 * a);
        return `The roots are complex: ${realPart} + ${imaginaryPart}i and ${realPart} - ${imaginaryPart}i`;
    }
}

// Example usage
let a = 1;
let b = -3;
let c = 2;

console.log(solveQuadratic(a, b, c)); // The roots are real and distinct: 2 and 1

a = 1;
b = -2;
c = 1;

console.log(solveQuadratic(a, b, c)); // The root is real and repeated: 1

a = 1;
b = 2;
c = 5;

console.log(solveQuadratic(a, b, c)); // The roots are complex: -1 + 2i and -1 - 2i
