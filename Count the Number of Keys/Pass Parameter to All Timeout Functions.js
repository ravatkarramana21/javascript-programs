function delayedFunction(param) {
    setTimeout(function() {
        console.log(param);
    }, 1000);
}

delayedFunction("Hello, World!");
