function getFileExtension(filename) {
    return filename.split('.').pop();
}

const filename = "example.txt";
const extension = getFileExtension(filename);

console.log(extension);
