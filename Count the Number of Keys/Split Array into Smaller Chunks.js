function chunkArray(array, size) {
    const result = [];
    for (let i = 0; i < array.length; i += size) {
        result.push(array.slice(i, i + size));
    }
    return result;
}

const array = [1, 2, 3, 4, 5, 6, 7, 8];
const chunks = chunkArray(array, 3);

console.log(chunks);
