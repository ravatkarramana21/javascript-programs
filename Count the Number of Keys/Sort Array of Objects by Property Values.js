const array = [
    { name: 'John', age: 25 },
    { name: 'Jane', age: 22 },
    { name: 'Jim', age: 27 }
];

array.sort((a, b) => a.age - b.age);

console.log(array);
