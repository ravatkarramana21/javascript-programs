function countVowels(str) {
    return str.match(/[aeiou]/gi)?.length || 0;
}

const str = "Hello, World!";
const vowelCount = countVowels(str);

console.log(vowelCount);
